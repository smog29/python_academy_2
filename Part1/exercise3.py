def print_name(name: str):
    print(name)


def print_sum(a: float, b: float, c: float):
    print(a + b + c)


def print_hello_world():
    print("Hello world")


def get_numbers_difference(a, b) -> float:
    return abs(a - b)


class Zombie:
    pass


def get_zombie_instance() -> Zombie:
    return Zombie()


print(isinstance(get_zombie_instance(), Zombie))
