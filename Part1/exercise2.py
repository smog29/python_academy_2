class Entity:
    pass


class Enemy(Entity):
    pass


class Zombie(Enemy):
    pass


class SmallZombie(Zombie):
    pass

