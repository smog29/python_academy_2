import random

# while loop

x = 0

while x != 20:
    x = random.randrange(0, 21)

# for loop

for i in range(20, -1, -1):
    print(i)
