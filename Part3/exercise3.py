# tuple of 100 elements

numbers = tuple((i for i in range(1, 101)))

# assigning tuple elements to variables

five_elements_tuple = (1, 2, 3, 4, 5)

u, w, x, y, z = five_elements_tuple

print(u, w, x, y, z)
