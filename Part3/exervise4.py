# dictionary a user fills

users_dictionary = {}


for i in range(5):
    key = input("Give a key for the dictionary ")

    while key in users_dictionary:
        key = input("You must give a unique key ")

    value = input("Give a value for the key ")

    users_dictionary[key] = value

# dictionary containing lists

d = {"flowers": ["rose", "lilies", "orchid", "hosta", "marigold"], "animals": ["dog", "bird", "lion", "cat", "tiger"],
     "furniture": ["table", "chair", "desk", "wardrobe", "bed"], "names": ["Kuba", "Jason", "Magda", "Rafal", "James"],
     "meals": ["pork chop", "soup", "chicken", "burger", "cereal"]}

for key, value in d.items():
    print(f"key: {key} has values: {value}")
