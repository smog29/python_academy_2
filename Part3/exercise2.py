# list of lists

ls = [[1], [2], [3]]


# empty list to fill

numbers = []

for i in range(100, 116):
    numbers.append(i)


# list of floats

floating_numbers = [1.3, 6.2, -5.3, 6.6, 0.01]

for i in floating_numbers:
    print(i)

