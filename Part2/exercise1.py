# inheriting variables
class Enemy:

    def __init__(self):
        self.x = 0
        self.y = 0


class Zombie(Enemy):

    def __init__(self):
        super().__init__()


# inheriting methods
class Shampoo:

    def __init__(self, name: str):
        self.name = name

    def wash_hair(self):
        print(f"Im washing hair with {self.name}")


class Schauma(Shampoo):

    def __init__(self):
        super().__init__("Schauma")


# super call to an overridden method
class Car:

    def __init__(self):
        pass

    def drive(self):
        print("Car is driving")


class Mercedes(Car):

    def __init__(self):
        super().__init__()

    def drive(self):
        print("Mercedes is driving")

    def driving_without_brand_name(self):
        super().drive()
