# comparing user's name to mine

def check_names():
    user_name = input("Give your name ")

    if user_name == "Kuba":
        print("Hello, You are cool")
    else:
        print("Hello")


# class with a comparison method

class Triangle:

    def __init__(self, a: float, b: float, c: float):
        self.a = a
        self.b = b
        self.c = c

        s = (self.a + self.b + self.c) / 2
        self.area = (s * (s - a) * (s - b) * (s - c)) ** 0.5

    def __eq__(self, other):
        if isinstance(Triangle, other):
            return self.a == other.a and self.b == other.b and self.c == other.c

        elif isinstance(float, other):
            return self.area == other


# class of a solid with comparing volume

class Cube:

    def __init__(self, a: float):
        self.a = a
        self.volume = a * a * a

    def __eq__(self, other):
        if isinstance(Cube, other):
            return self.volume == other.volume

        elif isinstance(float, other):
            return self.volume == other


# class tree
class Vehicle:

    def __init__(self):
        pass

    def __eq__(self, other):
        return isinstance(other, Vehicle)


class AirPlane(Vehicle):

    def __init__(self, weight: float, fuel_capacity: float):
        super().__init__()
        self.weight = weight
        self.fuel_capacity = fuel_capacity

    def __eq__(self, other):
        return self.weight == other.weight and self.fuel_capacity == other.fuel_capacity


class Boeing(AirPlane):

    def __init__(self, weight: float, fuel_capacity: float):
        super().__init__(weight, fuel_capacity)

    def __eq__(self, other):
        return super.__eq__(self, other)


class Car(Vehicle):

    def __init__(self, color: str, max_speed: float):
        super().__init__()
        self.color = color
        self.max_speed = max_speed

    def __eq__(self, other):
        return self.color == other.color and self.max_speed == other.max_speed


class BMW(Car):

    def __init__(self, color: str, max_speed: float):
        super().__init__(color, max_speed)

    def __eq__(self, other):
        return super().__eq__(self, other)


class Mercedes(Car):

    def __init__(self, color: str, max_speed: float):
        super().__init__(color, max_speed)

    def __eq__(self, other):
        return super().__eq__(other)


print(Mercedes("d", 12) == Mercedes("d", 12))
